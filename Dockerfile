FROM centos:7

USER root

RUN yum -y update && \
    yum install -y libXt

RUN mkdir -p /cvmfs /data

COPY piaastone_testcodes /src
COPY run_FH_analysis.sh /

USER user

ENTRYPOINT ["/run_FH_analysis.sh"]
