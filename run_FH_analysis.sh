#!/usr/bin/env bash

##########################################################
#                                                        #
#    This is a bash script to run the Frequency          #
#    Hough Continous Waves pipeline in a docker          #
#    container                                           #
#                                                        #
##########################################################

# First setup matlab to get the Matlab Compiler Runtime
. /cvmfs/oasis.opensciencegrid.org/osg/sw/module-init.sh
module load matlab/R2018b

if [ $# -eq 0 ]
then
    echo "No argument supplied"
    echo "Usage:"
    echo "    singularity run --bind /cvmfs --bind <Data base dir>:/data <image>" \
         " <Number of Injections> <PPs> <MODE_FH (normal or gpu)> <NFFTs> <verbose> <Detector (H,V or L)>"
    exit 1
fi

NINJ=$1
PPs=$2
MODE_FH=$3
NFFTS=$4
VERB=$5
DET=$6

if [[ ${MODE_FH} == 'normal' ]]
then
    EXEC=/src/FH_BSDpacchettoNOGPU/compiled/FH_analysisNOGPU
else
    EXEC=/src/FH_BSDpacchetto/compiled/FH_analysis
fi

${EXEC} /data ${NINJ} ${PPs} ${MODE_FH} ${NFFTS} ${VERB} ${DET}
